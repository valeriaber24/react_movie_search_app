import { useEffect, useState } from "react";
import styled from "styled-components";
import axios from 'axios';
const API_key = 'b6f3fed4';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    padding: 20px 30px;
    justify-content: center;
    border-bottom: 1px solid lightgray;
`;

const CoverImage = styled.img`
    object-fit: cover;
    height: 352px;
    box-shadow: 0 3px 10px 0 #aaa;
    border-radius: 5px;
`;

const InfoColumn = styled.div`
    display: flex;
    flex-direction: column;
    margin: 20px;
`;

const MovieName = styled.span`
    font-size: 18px;
    font-weight: 600;
    color: black;
    margin: 15px 0;
    text-align: center;
`;

const MovieInfo = styled.span`
    font-size: 16px;
    font-weight: 500;
    color: black;
    text-transform: capitalize;
`;

const Close = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: red;
    height: fit-content;
    cursor: pointer;
    text-shadow:2px 2px 5px gray;
`;

const MovieInfoComponent = (props) => {
    const [movieInfo, setMovieInfo] = useState();
    const { selectedMovie } = props;

    useEffect(() => {
        axios.get(`http://www.omdbapi.com/?i=${selectedMovie}&apikey=${API_key}&`)
            .then((response) => setMovieInfo(response.data));
    }, [selectedMovie]);

    return (
        <Container>
            {movieInfo ? <>
                <CoverImage src={movieInfo?.Poster} />
                <InfoColumn>
                    <MovieName>{movieInfo?.Type}: {movieInfo?.Title}</MovieName>
                    <MovieInfo><b>IMDB Rating:</b> {movieInfo?.imdbRating}</MovieInfo>
                    <MovieInfo><b>Year:</b> {movieInfo?.Year}</MovieInfo>
                    <MovieInfo><b>Language:</b> {movieInfo?.Language}</MovieInfo>
                    <MovieInfo><b>Rated:</b> {movieInfo?.Rated}</MovieInfo>
                    <MovieInfo><b>Released:</b> {movieInfo?.Released}</MovieInfo>
                    <MovieInfo><b>Runtime:</b> {movieInfo?.Runtime}</MovieInfo>
                    <MovieInfo><b>Genre:</b> {movieInfo?.Genre}</MovieInfo>
                    <MovieInfo><b>Director:</b> {movieInfo?.Director}</MovieInfo>
                    <MovieInfo><b>Actors:</b> {movieInfo?.Actors}</MovieInfo>
                    <MovieInfo><b>Plot:</b> {movieInfo?.Plot}</MovieInfo>
                </InfoColumn>
                <Close onClick={() => props.onMovieSelect()}>X</Close>
            </> : "Loading..."}
        </Container>
    );
}

export default MovieInfoComponent;